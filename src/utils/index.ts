import { tDimentions } from '../interfaces'

export const getRandomMine = (): boolean => {
    return Math.random() < 0.5 ? false : true
}

export const getNewMine = (dimentions: tDimentions): tDimentions => {
    return [random(0, dimentions[0]-1), random(0, dimentions[1]-1)]
}

export const random = (min: number, max: number): number => {
    return Math.floor(Math.random() * (max - min)) + min
}
