import { AnyAction } from "redux"

import { tLevel } from "../interfaces"

import { SET_GAME_IN_PROGRESS, SET_TIMER, INCREMENT_TIMER, SET_GAME_WON, SET_LEVEL } from './constants'

interface appState {
    // dimentions: [number, number],
    gameInProgress: boolean,
    gameWon: boolean,
    timer: number,
    level: tLevel
}

const initialState: appState = {
    // dimentions: [10, 10],
    gameInProgress: false,
    gameWon: false,
    timer: 0,
    level: 'easy'
}

export const appReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case SET_GAME_IN_PROGRESS: return { ...state, gameInProgress: action.payload }
        case SET_TIMER: return { ...state, timer: action.payload }
        case INCREMENT_TIMER: return { ...state, timer: state.timer+1 }
        case SET_GAME_WON: return { ...state, gameWon: action.payload }
        case SET_LEVEL: return { ...state, level: action.payload }
        default: return state
    }
}
