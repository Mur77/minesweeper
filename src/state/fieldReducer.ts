import { Action, AnyAction } from 'redux'

import { getNewMine } from '../utils'
import { 
    INIT_FIELD, 
    OPEN_MINE, 
    CLOSE_MINE, 
    OPEN_NEIGHBOURS, 
    SET_FLAG, 
    INC_OPENED_CELLS, 
    SET_FLAGS_ON_MINES, 
    SET_DIMENTIONS, 
    MINES_OPEN, 
    MINES_CLOSE, 
    INC_DEC_MINES_LEFT, 
    INIT_MINES_LEFT, 
    SET_MINES_COUNT
} from './constants'
import { tDimentions, iMine, iMineCoordinate } from '../interfaces'
import { rootReducer } from './rootReducer'

interface iInitialState {
    field: iMine[][]
    fieldShouldInitialize: boolean
    cellsOpened: number
    dimentions: tDimentions
    // Сколько всего мин
    minesCount: number
    // Сколько мин осталось открыть (при установке флагов)
    minesLeft: number
    // Если true - открываем все мины (при нажатии на закрытую мину)
    minesOpen: boolean
}

// --- Инициализация минного поля ---
export const initField = (dimentions: tDimentions, minesCount: number) => {
    let count = 0;

    // Инициализация поля
    const mineArray = new Array<iMine[]>(dimentions[1])

    for (let i=0; i<dimentions[1]; i++) {
        mineArray[i] = new Array<iMine>(dimentions[0])
        for (let j=0; j<dimentions[0]; j++) {
            mineArray[i][j] = { mine: false, neighbours: 0, opened: false, flag: false, x: i, y: j }
        }
    }

    // Инициализация мин
    while (count < minesCount) {
        const [x, y] = getNewMine(dimentions)
        if (mineArray[y][x].mine === false) {
            mineArray[y][x].mine = true
            count += 1
        }
    }

    // Подсчитываем соседей
    const matrix = [[-1,-1], [-1,0], [-1,1], [0,1], [1,1], [1,0], [1,-1], [0,-1]]

    for (let i=0; i<dimentions[1]; i++) {
        for (let j=0; j<dimentions[0]; j++) {
            count = 0

            // eslint-disable-next-line no-loop-func
            matrix.forEach(item => {
                const testI = i+item[1]
                const testJ = j+item[0]
                // Проверяем, не выходим ли за поле
                if ( (testI >= 0 && testI < dimentions[1]) && (testJ >= 0 && testJ < dimentions[0]) ) {
                    if (mineArray[testI][testJ].mine === true) {
                        count += 1
                    }
                }
            })
        
            mineArray[i][j].neighbours = count
        }
    }

    return mineArray
}

// --- Открываем/закрываем мину с заданными координатами ---
function setMine(field: iMine[][], coordinate: iMineCoordinate, status: boolean) {
    const newField = Object.assign<iMine[][], iMine[][]>([], field)
    const {x, y} = coordinate

    newField[y][x].opened = status

    return newField
}

// --- Открываем соседей мины с заданными координатами ---
function openNeighbours(field: iMine[][], dimentions: tDimentions, coordinate: iMineCoordinate, cellsOpened: number) {
    const matrix = [[-1,-1], [-1,0], [-1,1], [0,1], [1,1], [1,0], [1,-1], [0,-1]]

    const newField: iMine[][] = Object.assign<iMine[][], iMine[][]>([], field)

    const recursive = (coordinate: iMineCoordinate) => {
        const {x, y} = coordinate
        matrix.forEach(item => {
            const testX = x+item[0]
            const testY = y+item[1]
            // Проверяем, не выходим ли за поле
            if ( (testX >= 0 && testX < dimentions[0]) && (testY >= 0 && testY < dimentions[1]) ) {
                // Если сосед согласно матрице закрыт и нет флага
                if (newField[testY][testX].opened === false && newField[testY][testX].flag === false) {
                    // Открываем его
                    newField[testY][testX].opened = true
                    cellsOpened = cellsOpened+1
                    if (newField[testY][testX].neighbours === 0) {
                        // Если он равен 0, обрабатываем его рекурсивно
                        recursive({ x: testX, y: testY })
                    }
                }
            }
        })
    }

    recursive(coordinate)

    return { newField, cellsOpened }
}

// --- Установка флага на мину с заданными координатами ---
function setFlag(field: iMine[][], payload: any) {
    const newField = Object.assign<iMine[][], iMine[][]>([], field)
    const { coordinate, flag } = payload
    const { x, y } = coordinate

    newField[y][x].flag = flag

    return newField
}

// --- Установка флагов на все мины (когда все остальные клетки открыты) ---
function setFlagsOnMines(field: iMine[][], dimentions: tDimentions) {
    const newField = Object.assign<iMine[][], iMine[][]>([], field)

    for (let i=0; i<dimentions[0]; i++) {
        for (let j=0; j<dimentions[1]; j++) {
            if (newField[j][i].mine === true) {
                newField[j][i].flag = true
            }
        }
    }

    return newField
}

const initialDimentions: tDimentions = [10, 10]
const initialMinesCount = 15

const initialState: iInitialState = {
    field: initField(initialDimentions, initialMinesCount),
    // Если true - требуется инициализация поля
    fieldShouldInitialize: false,
    // Сколько открыто клеток (для выявления момента когда остались одни мины)
    cellsOpened: 0,
    dimentions: initialDimentions,
    minesCount: initialMinesCount,
    minesLeft: 0,
    minesOpen: false,
}

export const fieldReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case INIT_FIELD: {
            const newField = initField(state.dimentions, state.minesCount)
            return { ...state, field: newField, fieldShouldInitialize: false, cellsOpened: 0 }
        }
        case OPEN_MINE: {
            const newField = setMine(state.field, action.payload, true)
            return { ...state, field: newField, cellsOpened: state.cellsOpened+1 }
        }
        case CLOSE_MINE: {
            const newField = setMine(state.field, action.payload, false)
            return { ...state, field: newField, cellsOpened: state.cellsOpened-1 }
        }
        case OPEN_NEIGHBOURS: {
            const newField = openNeighbours(state.field, state.dimentions, action.payload, state.cellsOpened)
            return { ...state, field: newField.newField, cellsOpened: newField.cellsOpened }
        }
        case SET_FLAG: {
            const newField = setFlag(state.field, action.payload)
            return { ...state, field: newField }
        }
        case INC_OPENED_CELLS: {
            return { ...state, cellsOpened: state.cellsOpened+1 }
        }
        case SET_FLAGS_ON_MINES: {
            const newField = setFlagsOnMines(state.field, state.dimentions)
            return { ...state, field: newField }
        }
        case SET_DIMENTIONS: {
            return { ...state, dimentions: action.payload }
        }
        case MINES_OPEN: return { ...state, minesOpen: true }
        case MINES_CLOSE: return { ...state, minesOpen: false }
        case INC_DEC_MINES_LEFT: {
            // payload: true - уменьшаем minesLeft, false - увеличиваем
            if (action.payload === true) {
                if (state.minesLeft > 0) {
                    return { ...state, minesLeft: state.minesLeft-1 }
                }
            } else if (state.minesLeft < state.minesCount) {
                return { ...state, minesLeft: state.minesLeft+1 }
            }
            return state
        }
        case INIT_MINES_LEFT: return { ...state, minesLeft: state.minesCount }
        case SET_MINES_COUNT: return { ...state, minesCount: action.payload }
        default: return state
    }
}
