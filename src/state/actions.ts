import { AnyAction } from 'redux'

import { store } from './store'
import { 
    INIT_FIELD, 
    MINES_OPEN, 
    MINES_CLOSE,
    SET_GAME_IN_PROGRESS,
    SET_TIMER,
    INCREMENT_TIMER,
    OPEN_MINE,
    CLOSE_MINE,
    OPEN_NEIGHBOURS,
    SET_FLAG,
    INC_OPENED_CELLS,
    INC_DEC_MINES_LEFT,
    SET_FLAGS_ON_MINES,
    INIT_MINES_LEFT,
    SET_GAME_WON,
    SET_DIMENTIONS,
    SET_MINES_COUNT,
    SET_LEVEL
} from './constants'
import { iMineCoordinate, tDimentions } from '../interfaces'

export function initMinesLeft(): AnyAction {
    return { type: INIT_MINES_LEFT }
}
export function initField(): AnyAction {
    store.dispatch(initMinesLeft())
    store.dispatch(setGameWon(false))
    return { type: INIT_FIELD }
}

export function setMinesOpen(): AnyAction {
    return { type: MINES_OPEN }
}

export function minesClose(): AnyAction {
    return { type: MINES_CLOSE }
}

export function setGameInProgress(status: boolean): AnyAction {
    return { type: SET_GAME_IN_PROGRESS, payload: status }
}

export function setTimer(timer: number): AnyAction {
    return { type: SET_TIMER, payload: timer }
}

export function incrementTimer(): AnyAction {
    return { type: INCREMENT_TIMER }
}

export function openMine(coordinate: iMineCoordinate): AnyAction {
    return { type: OPEN_MINE, payload: coordinate }
}

export function closeMine(coordinate: iMineCoordinate): AnyAction {
    return { type: CLOSE_MINE, payload: coordinate }
}

export function openNeighbours(coordinate: iMineCoordinate): AnyAction {
    return { type: OPEN_NEIGHBOURS, payload: coordinate }
}

export function incDecMinesLeft(status: boolean): AnyAction {
    // status: true - уменьшаем minesLeft, false - увеличиваем
    return { type: INC_DEC_MINES_LEFT, payload: status }
}

export function setFlag(coordinate: iMineCoordinate, flag: boolean): AnyAction {
    store.dispatch(incDecMinesLeft(flag))
    return { type: SET_FLAG, payload: { coordinate, flag }}
}

export function incOpenedCells(): AnyAction {
    return { type: INC_OPENED_CELLS }
}

export function setFlagsOnMines(): AnyAction {
    return { type: SET_FLAGS_ON_MINES }
}

export function setGameWon(status: boolean): AnyAction {
    return { type: SET_GAME_WON, payload: status }
}

export function setDimentions(newDimentions: tDimentions): AnyAction {
    return { type: SET_DIMENTIONS, payload: newDimentions }
}

export function setMinesCount(newMinesCount: number): AnyAction {
    return { type: SET_MINES_COUNT, payload: newMinesCount }
}

export function setLevel(level: string) {
    return { type: SET_LEVEL, payload: level }
}
