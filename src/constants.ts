import { iLevel } from './interfaces'

export const LEVELS: iLevel[] = [
    {
        level: 'easy',
        dimentions: [10, 10],
        minesCount: 15,
    },
    {
        level: 'medium',
        dimentions: [20, 20],
        minesCount: 40,
    },
    {
        level: 'hard',
        dimentions: [50, 20],
        minesCount: 130,
    },
    {
        level: 'custom',
        dimentions: [0, 0],
        minesCount: 0,
    },
]
