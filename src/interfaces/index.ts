export type tDimentions = [number, number]

export type tLevel = 'easy' | 'medium' | 'hard' | 'custom'

export interface iLevel {
    level: tLevel
    dimentions: tDimentions
    minesCount: number
}
export interface iMineCoordinate {
    x: number
    y: number
}
export interface iMine {
    mine: boolean
    neighbours: number
    opened: boolean
    flag: boolean
    x: number
    y: number
}
