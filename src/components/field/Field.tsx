import React, { useEffect } from 'react'

import { Infobar } from '../infobar/Infobar'
import { Mine } from '../mine/Mine'
import { ParamsSelector } from '../paramsSelector/ParamsSelector'

import { useAppSelector, useAppDispatch } from '../../state/hooks'
import { initField } from '../../state/actions'

import styles from './Field.module.scss'

export const Field = () => {
    const dispatch = useAppDispatch()
    const field = useAppSelector(state => state.field.field)
    const fieldShouldInitialize = useAppSelector(state => state.field.fieldShouldInitialize)

    useEffect(() => {
        dispatch(initField())
    }, [fieldShouldInitialize])

    return (
        <div className={styles.borderContainer}>
            <div className={styles.container}>
                <Infobar />
                {field.map((item, index) => {
                    return (
                        <div key={`line${index}`} className={styles.lineContainer}>
                            {item.map((item1, index1) => {
                                return (
                                    <Mine
                                        key={`mine${index}.${index1}`}
                                        mine={item1.mine}
                                        neighbours={item1.neighbours}
                                        opened={item1.opened}
                                        flag={item1.flag}
                                        x={index1}
                                        y={index}
                                    />
                                )
                            })}
                        </div>
                    )
                })}
                <ParamsSelector />
            </div>
        </div>
    )
}
