import React, { useState } from 'react'
import clsx from 'clsx'

import { useAppDispatch, useAppSelector } from '../../state/hooks'
import { initField, minesClose, setDimentions, setGameInProgress, setLevel, setMinesCount, setTimer } from '../../state/actions'
import { CustomButton } from '../customButton/CustomButton'
import { LEVELS } from '../../constants'

import ArrowDown from '../../img/icons/arrowdown.svg'
import ArrowUp from '../../img/icons/arrowup.svg'
import SetIcon from '../../img/icons/done.svg'

import styles from './ParamsSelector.module.scss'

interface iFormError {
    width: boolean
    height: boolean
    mines: boolean
}

export const ParamsSelector = () => {
    const [visible, setVisible] = useState(false)
    const [error, setError] = useState<iFormError>({
        width: false,
        height: false,
        mines: false,
    })

    const dispatch = useAppDispatch()
    const dimentions = useAppSelector(state => state.field.dimentions)
    const level = useAppSelector(state => state.app.level)
    const minesCount = useAppSelector(state => state.field.minesCount)

    const toggleParams = () => {
        setVisible(!visible)
    }

    const handleRadioClick = (e: any) => {
        const newLevel = LEVELS.find(item => item.level === e.target.value)
        dispatch(setLevel(newLevel!.level))
        dispatch(setDimentions(newLevel!.dimentions))
        dispatch(setMinesCount(newLevel!.minesCount))
    }

    const handleSetClick = () => {
        // Проверка введенных значений
        if (level === 'custom') {
            const formError: iFormError = {
                width: false,
                height: false,
                mines: false,                
            }
            let flag = false

            if (dimentions[0] < 10 || dimentions[0] > 50) {
                formError.width = true
                flag = true
            }
            if (dimentions[1] < 10 || dimentions[1] > 20) {
                formError.height = true
                flag = true
            }
            if (!formError.width && !formError.height) {
                const minMinesCount = Math.trunc(dimentions[0] * dimentions[1] / 10)
                const maxMinesCount = Math.trunc(dimentions[0] * dimentions[1] / 5)
                if (minesCount < minMinesCount || minesCount > maxMinesCount) {
                    formError.mines = true
                    flag = true
                }
            }

            if (flag) {
                setError(formError)
                return
            }
        }

        setVisible(false)
        setError({
            width: false,
            height: false,
            mines: false,                
        })
        dispatch(minesClose())
        dispatch(initField())
        dispatch(setGameInProgress(false))
        dispatch(setTimer(0))
    }

    const handleChange = (e: any) => {
        switch(e.target.name) {
            case 'width': {
                dispatch(setDimentions([e.target.value, dimentions[1]]))
                return
            }
            case 'height': {
                dispatch(setDimentions([dimentions[0], e.target.value]))
                return
            }
            case 'mines': {
                dispatch(setMinesCount(e.target.value))
                return
            }
            default: {
                return
            }
        }
    }

    return (
        <div className={styles.container}>
            {visible ? (
                <>
                    <div className={styles.expandCollapseIcon} onClick={toggleParams}>
                        <ArrowUp />
                    </div>
                    <div className={styles.form}>
                        <div className={styles.rightSide}>
                            <label className={styles.formRadioElement}>
                                <input type="radio" id="easy" name="param" value="easy" checked={level === 'easy'} onClick={handleRadioClick} readOnly />
                                Easy
                            </label>
                            <label className={styles.formRadioElement}>
                                <input type="radio" id="medium" name="param" value="medium" checked={level === 'medium'} onClick={handleRadioClick} readOnly />
                                Medium
                            </label>
                            <label className={styles.formRadioElement}>
                                <input type="radio" id="hard" name="param" value="hard" checked={level === 'hard'} onClick={handleRadioClick} readOnly />
                                Hard
                            </label>
                            <label className={styles.formRadioElement}>
                                <input type="radio" id="custom" name="param" value="custom" checked={level === 'custom'} onClick={handleRadioClick} readOnly />
                                Custom
                            </label>
                        </div>
                        <div>
                            <label className={styles.formInputLabel}>
                                Witdh
                                <input 
                                    className={clsx(styles.formInputElement, {
                                        [styles.formError]: error.width,
                                    })}
                                    name="width"
                                    value={dimentions[0]}
                                    disabled={level !== 'custom'}
                                    onChange={handleChange}
                                />
                            </label>
                            <label className={styles.formInputLabel}>
                                Height
                                <input 
                                    className={clsx(styles.formInputElement, {
                                        [styles.formError]: error.height,
                                    })}                                    
                                    name="height"
                                    value={dimentions[1]}
                                    disabled={level !== 'custom'}
                                    onChange={handleChange}
                                />
                            </label>
                            <label className={styles.formInputLabel}>
                                Mines count
                                <input
                                    className={clsx(styles.formInputElement, {
                                        [styles.formError]: error.mines,
                                    })}
                                    name="mines"
                                    value={minesCount}
                                    disabled={level !== 'custom'}
                                    onChange={handleChange}
                                />
                            </label>
                        </div>
                    </div>
                    <div className={styles.setButton}>
                        <CustomButton iconTrue={<SetIcon />} handleClick={handleSetClick} />
                    </div>                    
                </>
            ) : (
                <div className={styles.expandCollapseIcon} onClick={toggleParams}>
                    <ArrowDown />
                </div>
            )}
        </div>
    )
}
