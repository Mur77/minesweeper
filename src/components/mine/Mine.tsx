import React, { useEffect, useState } from 'react'
import clsx from 'clsx'

import { useAppSelector, useAppDispatch } from '../../state/hooks'
import { 
    openMine, 
    closeMine, 
    setGameInProgress, 
    setMinesOpen, 
    openNeighbours, 
    setFlag,
    setFlagsOnMines, 
    setGameWon 
} from '../../state/actions'

import { iMine } from '../../interfaces'
import MineSvg from '../../img/icons/mine.svg'
import FlagSvg from '../../img/icons/flag.svg'

import styles from './Mine.module.scss'

export const Mine: React.FC<iMine> = ({ mine, neighbours, opened, flag, x, y }) => {
    const dispatch = useAppDispatch()
    // minesOpen = true, если все мины открыты (нарвались на мину)
    const minesOpen = useAppSelector(state => state.field.minesOpen)
    const gameInProgress = useAppSelector(state => state.app.gameInProgress)
    const fieldShouldInitialize = useAppSelector(state => state.field.fieldShouldInitialize)
    const cellsOpened = useAppSelector(state => state.field.cellsOpened)
    const dimentions = useAppSelector(state => state.field.dimentions)
    const minesCount = useAppSelector(state => state.field.minesCount)
    // gameWon = true, когда игра выиграна
    const gameWon = useAppSelector(state => state.app.gameWon)

    const [mouseDown, setMouseDown] = useState(false)

    const handleMouseUpDown = (e: React.MouseEvent) => {
        switch (e.button) {
            case 0: {
                if (!minesOpen && !gameWon) {
                    if (e.type === 'mousedown') {
                        setMouseDown(true)
                    }
                    if (e.type === 'mouseup' && mouseDown) {
                        setMouseDown(false)
                        if (!gameInProgress) {
                            dispatch(setGameInProgress(true))
                        }
                        dispatch(openMine({x, y}))
                        if (mine && !opened && !minesOpen) {
                            dispatch(setMinesOpen())
                            dispatch(setGameInProgress(false))
                        } else if (neighbours === 0) {
                            dispatch(openNeighbours({x, y}))
                        }
                        if (dimentions[0]*dimentions[1]-minesCount-1 <= cellsOpened) {
                            dispatch(setFlagsOnMines())
                            dispatch(setGameInProgress(false))
                            dispatch(setGameWon(true))
                        }
                    }
                }
                return
            }
            case 2: {
                if (e.type === 'mousedown') {
                    if (!opened && !minesOpen && !flag) {
                        dispatch(setFlag({x, y}, true))
                    } else if (flag) {
                        dispatch(setFlag({x, y}, false))
                    }
                }
                return
            }
            default: {
                return
            }
        }
    }

    const handleContextMenu = (e: React.MouseEvent) => {
        e.preventDefault()
    }

    const handleMouseLeave = (e: React.MouseEvent) => {
        setMouseDown(false)
    }

    useEffect(() => {
        if (fieldShouldInitialize) {
            dispatch(closeMine({x, y}))
            dispatch(setFlag({x, y}, false))
        }
    }, [fieldShouldInitialize])

    let render
    if (minesOpen && mine) {
        render = <MineSvg />
    } else if (opened && !fieldShouldInitialize) {
        if (mine) {
            render = <MineSvg />
        } else {
            render = (
                <span className={clsx({
                    [styles.one]: neighbours === 1, 
                    [styles.two]: neighbours === 2, 
                    [styles.three]: neighbours === 3, 
                    [styles.four]: neighbours === 4, 
                    [styles.five]: neighbours === 5, 
                    [styles.six]: neighbours === 6, 
                    [styles.seven]: neighbours === 7, 
                    [styles.eight]: neighbours === 8, 
                })}>
                    {neighbours === 0 ? '' : neighbours}
                </span>
            )
        }
    } else if (flag) {
        render = <FlagSvg />
    } else {
        render = ''
    }

    return (
        <>
            <div 
                className={clsx(styles.container, {
                    [styles.normal]: !mouseDown && !opened,
                    [styles.mouseDown]: mouseDown,
                    [styles.opened]: opened,
                })}
                onMouseDown={handleMouseUpDown}
                onMouseUp={handleMouseUpDown}
                onContextMenu={handleContextMenu}
                onMouseLeave={handleMouseLeave}
            >
                {render}
            </div>
        </>
    )
}
