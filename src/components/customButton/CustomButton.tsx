import React, { ReactNode, useState } from 'react'
import clsx from 'clsx'

import styles from './CustomButton.module.scss'

interface iCustomButton {
    handleClick: () => void
    iconTrue: ReactNode
    iconFalse?: ReactNode
    iconSwitch?: boolean
}

export const CustomButton: React.FC<iCustomButton> = ({
    handleClick, iconTrue, iconFalse, iconSwitch = true
}) => {
    const [pressed, setPressed] = useState(false)

    const handleMouseUpDown = (e: React.MouseEvent) => {
        if (e.type === 'mousedown') {
            setPressed(true)
        }
        if (e.type === 'mouseup') {
            setPressed(false)
        }
    }

    const handleMouseLeave = () => {
        setPressed(false)
    }

    return (
        <div
            className={clsx(styles.container, {
                [styles.normal]: !pressed,
                [styles.pressed]: pressed,
            })}
            onMouseDown={handleMouseUpDown}
            onMouseUp={handleMouseUpDown}
            onMouseLeave={handleMouseLeave}
            onClick={handleClick}
        >
            {iconSwitch ? (
                <>
                    {iconTrue}
                </>
            ) : (
                <>
                    {iconFalse}
                </>
            )}
        </div>
    )
}
