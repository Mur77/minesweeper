import React from 'react'
import { useAppSelector, useAppDispatch } from '../../../state/hooks'
import { minesClose, initField, setGameInProgress, setTimer } from '../../../state/actions'

import { CustomButton } from '../../customButton/CustomButton'

import Reload from '../../../img/icons/reload.svg'
import Stop from '../../../img/icons/stop.svg'

export const Button = () => {
    const dispatch = useAppDispatch()
    const gameInProgress = useAppSelector(state => state.app.gameInProgress)
    const minesOpen = useAppSelector(state => state.field.minesOpen)

    const handleClick = () => {
        if (!gameInProgress) {
            dispatch(minesClose())
            dispatch(initField())
            dispatch(setGameInProgress(false))
            dispatch(setTimer(0))
        } else {
            dispatch(setGameInProgress(false))
        }
    }

    return (
        <CustomButton 
            handleClick={handleClick}
            iconTrue={<Stop />}
            iconFalse={<Reload />}
            iconSwitch={gameInProgress}
        />
    )
}
