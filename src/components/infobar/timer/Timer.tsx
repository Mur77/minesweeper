import React, { useEffect } from 'react'

//import { useGlobalTimer, useGlobalGameInProgress } from '../../../state'
import { useAppSelector, useAppDispatch } from '../../../state/hooks'
import { incrementTimer } from '../../../state/actions'

import styles from './Timer.module.scss'

export const Timer = () => {
    const dispatch = useAppDispatch()
    const timer = useAppSelector(state => state.app.timer)
    const gameInProgress = useAppSelector(state => state.app.gameInProgress)

    useEffect(() => {
        let interval

        if (gameInProgress) {
            interval = setInterval(() => {
                dispatch(incrementTimer())
            }, 1000)
        }

        return () => clearInterval(interval)
    }, [timer, gameInProgress])

    return (
        <div className={styles.container}>
            <span>{timer}</span>
        </div>
    )
}
