import React from 'react'

import { Timer } from './timer/Timer'
import { Button } from './button/Button'
import { MinesLeft } from './minesLeft/MinesLeft'

import styles from './Infobar.module.scss'

export const Infobar = () => {
    return (
        <div className={styles.container}>
            <div className={styles.item1}><Timer /></div>
            <div className={styles.item2}><MinesLeft /></div>
            <div className={styles.item3}><Button /></div>
        </div>
    )
}
