import React from 'react'
import { useAppSelector } from '../../../state/hooks'

import MineSvg from '../../../img/icons/mine.svg'

import styles from './MinesLeft.module.scss'

export const MinesLeft = () => {
    const minesLeft = useAppSelector(state => state.field.minesLeft)
    
    return (
        <div className={styles.container}>
            <MineSvg />
            <div className={styles.number}>
                &nbsp;{minesLeft}
            </div>
        </div>
    )
}
